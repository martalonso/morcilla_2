$(document).ready(function(){
    init();

});

var data;
var search = document.getElementById('search');     

function init(){

    search = document.getElementById('search');
    $('#search').on({
        keyup: function() {
             val= $(this).val();
              if(val.length >= 3){
              //if(e.which == 13) {       
                $.ajax({
                    type: 'POST',
                    url : 'functions.php',
                    dataType : 'json',
                    data: 'search='+val+'&action=search',
                    success:function(response){
                        $('#content').html(response.output);
                        actions();    
                    }, 
                    error: function( data, textStatus, jqXHR){
                        console.log(data);
                        $('#content').html('<p> Error search </p>');
                    }
                });
                return false;
              }
        },
        keypress: function(e) {
            val= $(this).val();
           if(e.which == 13){     
            $.ajax({
                type: 'POST',
                url : 'functions.php',
                dataType : 'json',
                data: 'search='+val+'&action=search',
                success:function(response){
                    $('#content').html(response.output);
                    actions();    
                }, 
                error: function( data, textStatus, jqXHR){
                    console.log(data);
                    $('#content').html('<p> Error search </p>');
                }
            });
            return false;
          }
        }
    });

} // END INIT

function actions(){

    // NEW DATA
    $('#newdata').submit(function(e) {
        
        data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: 'functions.php',
            dataType: 'json',
            data: data+'&action=new-data',
            success: function(response){
                $('#content').html(response.output);
                actions();
            },
            error: function(data, textStatus, jqXHR){
                console.log(data);
                $('#content').html('<p>Error añadir data</p>');
            }
        });
        return false;
    });

    // EDIT DATA LINK 
    $('.editlink').click(function(e) {
    
       // var search = $('#search').val();
       // var name = search.split('-');
        var row = $(this).parent().parent();
        //var client= $.trim(name[0]);

        var client = $(this).closest('div').find('span').text();

        $(row).each(function() { 
            data = $(this).find('input').serialize();
            data += '&client='+client;
        });
        //console.log(values);
       $.ajax({
            type: 'POST',
            url: 'functions.php',
            dataType: 'json',
            data: data+'&action=edit',
            success: function(response){
                //console.log(response);
                $('#content').html(response.output);
                actions();
            },
            error: function(data, textStatus, jqXHR){
                console.log(data);
                $('#content').html('<p>Error editar data</p>');
            }

        });
        return false;
    });

    // NEW CLIENT
    $('#clientform').submit(function(e) {
        var search = $('#search').val();
        var name = search.split('-');
        var client = $.trim(name[0]);
        data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: 'functions.php',
            dataType: 'json',
            data: data+'&action=new-client',
            success: function(response){
                $('#content').html(response.output);
                actions();
            },
            error: function(data, textStatus, jqXHR){
                console.log(data);
                $('#content').html('<p>Error añadir data</p>');
            }

        });
        return false;
    });

    // EDIT CLIENT LINK 
    $('.editclient').click(function(e) {

        var client = $(this).closest('h4').find('span').text();
        //console.log(client);
        
        $.ajax({
            type: 'POST',
            url: 'functions.php',
            dataType: 'json',
            data: 'client='+client+'&action=editclient',
            success: function(response){
                $('#content').html(response.output);
                actions();
            },
            error: function(data, textStatus, jqXHR){
                console.log(data);
                $('#content').html('<p>Error editar cliente</p>');
            }

        });
        return false;
    });

    // EDIT CLIENT 
    $('#editclient').submit(function(e) {
        
        data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: 'functions.php',
            dataType: 'json',
            data: data+'&action=edit-client',
            success: function(response){
                //console.log(response);
                $('#content').html(response.output);
                actions();
            },
            error: function(data, textStatus, jqXHR){
                console.log(data);
                $('#content').html('<p>Error editar cliente</p>');
            }

        });
        return false;
    });

    // DELETE DATA
    $('.deletelink').on('click', function(event) {
        event.preventDefault();
        type = $(this).closest('ul').find('input[name="type"]').val();
        var client = $(this).closest('div').find('span').text();

        var r = confirm("Estás seguro que quieres borrar "+type+" de "+client+" ?");
        if (r == true) {
           // var search = $('#search').val();
            //var name = search.split('-');
            var row = $(this).parent().parent();
            var data = new Array();
            //var client = $.trim(name[0]);
             
            $(row).each(function() { 
                values = $(this).find('input').serialize(); 
                values += '&client='+client;
            });
           // console.log(values);
           $.ajax({
                type: 'POST',
                url: 'functions.php',
                dataType: 'json',
                data: values+'&action=delete',
                success: function(response){
                    $('#content').html(response.output);
                    actions();
                },
                error: function(data, textStatus, jqXHR){
                    $('#content').html('<p>Error borrar data</p>');
                }

            });
            return false;
        } else {
            return false;
        }
    });

    // back
    $('button[name="btn-back"]').on('click',function() {
         self.location.reload();
    });

}; // END ACTIONS


