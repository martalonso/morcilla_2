<p class="pleft"> No se ha encontrado <strong>" <?php echo $name ?>"</strong></p>
<h4>Agregar nuevo Cliente</h4>
<form id="clientform" method="POST" action="../functions.php">
    <div>
        <label for="cliente">Cliente</label>
        <input type="text" name="client" list="clients" value="<?php echo $name ?>" required/>
        <datalist id="clients"><?php getClients(); ?></datalist>
    </div>
    <div>
       <button type="submit" name="new-data">Añadir Datos</button>
    </div> 
</form>
