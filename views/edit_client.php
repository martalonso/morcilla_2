<h4>Datos a editar</h4>
<form id="editclient" method="POST" action="../functions.php">
  <div>
    <label for="oldname">Nombre actual:</label>
    <input type="text" name="client" value="<?php echo $name ?>" required>
  </div>
  <div>
    <label for="newname">Nuevo nombre:</label>
    <input type="text" name="newname" required>
  </div>
  <div>
		<button type="submit" name="edit-client">Editar cliente</button>
  </div>  
</form>