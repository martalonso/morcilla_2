<p class="pleft"> No se ha encontrado <strong>" <?php echo $type ?>"</strong> para : <strong><?php echo $name ?></strong></p>
<h4>Agregar nuevos datos</h4>
<form id="newdata" method="POST" action="../functions.php">
    <div>
        <label for="cliente">Cliente</label>
        <input type="text" name="client" value="<?php echo $name ?>" required/>
    </div>
    <div>
        <label for="tipo">Tipo</label>
        <input type="text" name="type" list="types" value="<?php echo $type ?>" required/>
        <datalist id="types"><?php getTypes(); ?></datalist>
    </div>
    <div>
        <label for="website">URL</label>
        <input type="text" name="website"/>
    </div>
    <div>
        <label for="user">User</label>
        <input ctype="text" name="user"/>
    </div>
    <div>
        <label for="password">Password</label>
        <input type="text" name="password"/>
    </div>
    <div>
       <button type="submit" name="new-data">Añadir Datos</button>
    </div> 
</form>
